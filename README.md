# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT ID = "10031"

## Project description
This is the obligatory task in the subject IDATT1003 about a train departure system.


## Project structure
Source code is located in the `src` folder. The `src` folder contains two packages, `train` and `train.test`. The `train` package contains the source code for the train departure system. The `train.test` package contains the source code for the unit tests.

## Link to repository
https://gitlab.stud.idi.ntnu.no/eskildsm/traindispatchsystemidatt1003

## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)
To run the program, compile the program. The main method of the application is located in the `src/main/java/edu.ntnu.stud/TrainDispatchApp.java` file. The program will run until the user exits the program.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)
Tests are located in the `src/test/java/edu.ntnu.stud` folder. To run the tests, compile the each of the tests and run them.