package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class TrainDepartureTest {
  @Nested
  @DisplayName("Positive tests for TrainDeparture constructor and setters")
  public class PositiveTests {
    TrainDeparture trainDeparture;

    @BeforeEach
    void init() {
      Time time = new Time(java.time.LocalTime.parse("00:00"));
      ValidationUtils validationUtils = new ValidationUtils(time);
      trainDeparture =
          new TrainDeparture(
              1, "line", "destination", java.time.LocalTime.parse("12:00"), validationUtils);
    }

    @Test
    @DisplayName("Test constructor with valid parameters")
    public void testConstructor() {
      assertEquals(1, trainDeparture.getTrainDepartureNumber());
      assertEquals("line", trainDeparture.getLine());
      assertEquals("destination", trainDeparture.getDestination());
      assertEquals(java.time.LocalTime.parse("12:00"), trainDeparture.getDepartureTime());
    }

    @Test
    @DisplayName("Test setTrack with valid parameter")
    public void testSetTrack() {
      trainDeparture.setTrack(1);
      assertEquals(1, trainDeparture.getTrack());
    }

    @Test
    @DisplayName("Test setDelay with valid parameter")
    public void testSetDelay() {
      trainDeparture.setDelay(java.time.LocalTime.parse("00:10"));
      assertEquals(java.time.LocalTime.parse("00:10"), trainDeparture.getDelay());
    }

    @Test
    @DisplayName("Test toString")
    public void testToString() {
      trainDeparture.setTrack(1);
      trainDeparture.setDelay(java.time.LocalTime.parse("00:10"));
      assertEquals(
          "Departure Time:12:00, Line:'line', Train Departure Number:1, Destination:'destination',"
              + " Delay:00:10, Track:1",
          trainDeparture.toString());
    }
  }

  @Nested
  @DisplayName("Negative tests for TrainDeparture constructor and setters")
  public class NegativeTestsWithNullParameters {
    ValidationUtils validationUtils;

    @BeforeEach
    void init() {
      Time time = new Time(java.time.LocalTime.parse("00:00"));
      validationUtils = new ValidationUtils(time);
    }

    @Test
    @DisplayName("Test constructor with null as parameters")
    public void constructorThrowsNullPointerException() {
      assertThrows(
          NullPointerException.class,
          () ->
              new TrainDeparture(
                  1, null, "destination", java.time.LocalTime.parse("12:00"), validationUtils));
      assertThrows(
          NullPointerException.class,
          () ->
              new TrainDeparture(
                  1, "line", null, java.time.LocalTime.parse("12:00"), validationUtils));
      assertThrows(
          NullPointerException.class,
          () -> new TrainDeparture(1, "line", "destination", null, validationUtils));
    }

    @Test
    @DisplayName("Test setDelay with null as parameter")
    public void setDelayThrowsNullPointerException() {
      TrainDeparture trainDeparture =
          new TrainDeparture(
              1, "line", "destination", java.time.LocalTime.parse("12:00"), validationUtils);
      assertThrows(
          NullPointerException.class,
          () -> {
            trainDeparture.setDelay(null);
          });
    }
  }

  @Nested
  @DisplayName(
      "Negative tests for TrainDeparture constructor and setters throws illegal argument exception")
  public class NegativeTestsWithIllegalArguments {
    ValidationUtils validationUtils;

    @BeforeEach
    void init() {
      Time time = new Time(java.time.LocalTime.parse("00:00"));
      validationUtils = new ValidationUtils(time);
    }

    @Test
    @DisplayName("Test constructor with zero trainDepartureNumber")
    public void constructorThrowsIllegalArgumentExceptionZeroTrainDepartureNumber() {
      assertThrows(
          IllegalArgumentException.class,
          () ->
              new TrainDeparture(
                  -1, "line", "destination", java.time.LocalTime.parse("12:00"), validationUtils));
    }

    @Test
    @DisplayName("Test constructor with validationUtils as null")
    public void constructorThrowsNullPointerExceptionValidationUtilsIsNull() {
      assertThrows(
          NullPointerException.class,
          () ->
              new TrainDeparture(
                  1, "line", "destination", java.time.LocalTime.parse("12:00"), null));
    }

    @Test
    @DisplayName("Test constructor with empty line")
    public void constructorThrowsIllegalArgumentExceptionEmptyLine() {
      assertThrows(
          IllegalArgumentException.class,
          () ->
              new TrainDeparture(
                  1, "", "destination", java.time.LocalTime.parse("12:00"), validationUtils));
    }

    @Test
    @DisplayName("Test constructor with empty destination")
    public void constructorThrowsIllegalArgumentExceptionEmptyDestination() {
      assertThrows(
          IllegalArgumentException.class,
          () ->
              new TrainDeparture(
                  1, "line", "", java.time.LocalTime.parse("12:00"), validationUtils));
    }

    @Test
    @DisplayName("Test setTrack with track as zero")
    public void setTrackThrowsIllegalArgumentExceptionTrackIsZero() {
      TrainDeparture trainDeparture =
          new TrainDeparture(
              1, "line", "destination", java.time.LocalTime.parse("12:00"), validationUtils);
      assertThrows(
          IllegalArgumentException.class,
          () -> {
            trainDeparture.setTrack(0);
          });
    }
  }
}
