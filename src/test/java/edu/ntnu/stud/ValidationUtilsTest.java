package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalTime;
import java.util.LinkedList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ValidationUtilsTest {
  @Nested
  @DisplayName("Positive tests for ValidationUtils")
  class PositiveTests {
    @Test
    @DisplayName(
        "validateDepartureNumber() should not throw an exception"
            + " when departureNumber is positive and unique")
    public void
        validateDepartureNumberShouldNotThrowExceptionWhenDepartureNumberIsPositiveAndUnique() {
      int departureNumber = 1;

      assertDoesNotThrow(() -> ValidationUtils.validateDepartureNumber(departureNumber));
    }

    @Test
    @DisplayName("validateLine() should not throw an exception when line is not empty")
    public void validateLineShouldNotThrowExceptionWhenLineIsNotEmpty() {
      String line = "L1";

      assertDoesNotThrow(() -> ValidationUtils.validateLine(line));
    }

    @Test
    @DisplayName(
        "validateDestination() should not throw an exception when destination is not empty")
    public void validateDestinationShouldNotThrowExceptionWhenDestinationIsNotEmpty() {
      String destination = "Trondheim";

      assertDoesNotThrow(() -> ValidationUtils.validateDestination(destination));
    }

    @Test
    @DisplayName("validateTime() should not throw an exception when time is not null")
    public void validateTimeShouldNotThrowExceptionWhenTimeIsNotNull() {
      Time time = new Time(LocalTime.parse("12:00"));
      ValidationUtils validationUtils = new ValidationUtils(time);

      assertDoesNotThrow(() -> validationUtils.validateTime(LocalTime.parse("10:00")));
    }

    @Test
    @DisplayName(
        "validateDepartureTime() should not throw an exception"
            + " when departureTime is not null and is after time")
    public void validateDepartureTimeShouldNotThrowExceptionWhenDepartureTimeIsNotNull() {
      Time time = new Time(LocalTime.parse("12:00"));
      ValidationUtils validationUtils = new ValidationUtils(time);

      assertDoesNotThrow(() -> validationUtils.validateDepartureTime(LocalTime.parse("13:00")));
    }

    @Test
    @DisplayName(
        "validateTrackNumber() should not throw an exception when trackNumber is greater than zero")
    public void validateTrackNumberShouldNotThrowExceptionWhenTrackNumberIsGreaterThanZero() {
      int trackNumber = 1;

      assertDoesNotThrow(() -> ValidationUtils.validateTrackNumber(trackNumber));
    }

    @Test
    @DisplayName(
        "validateTrainDeparture() should not throw an exception when trainDeparture is not null")
    public void validateTrainDepartureShouldNotThrowExceptionWhenTrainDepartureIsNotNull() {
      ValidationUtils validationUtils = new ValidationUtils(new Time(LocalTime.parse("12:00")));
      TrainDeparture trainDeparture =
          new TrainDeparture(1, "L1", "Trondheim", LocalTime.parse("12:00"), validationUtils);

      assertDoesNotThrow(() -> ValidationUtils.validateTrainDeparture(trainDeparture));
    }

    @Test
    @DisplayName(
        "validateDepartureNumberUnique() should not throw an"
            + " exception when departureNumber is unique")
    public void validateDepartureNumberUniqueShouldNotThrowExceptionWhenDepartureNumberIsUnique() {
      LinkedList<TrainDeparture> trainDepartures = new LinkedList<>();
      ValidationUtils validationUtils = new ValidationUtils(new Time(LocalTime.parse("12:00")));
      trainDepartures.add(
          new TrainDeparture(1, "L1", "Trondheim", LocalTime.parse("12:00"), validationUtils));
      trainDepartures.add(
          new TrainDeparture(2, "L1", "Trondheim", LocalTime.parse("12:00"), validationUtils));
      trainDepartures.add(
          new TrainDeparture(3, "L1", "Trondheim", LocalTime.parse("12:00"), validationUtils));

      assertDoesNotThrow(() -> ValidationUtils.validateDepartureNumberUnique(4, trainDepartures));
    }
  }

  @Nested
  @DisplayName("Negative tests for ValidationUtils")
  public class NegativeTests {
    @Test
    @DisplayName(
        "validateDepartureNumber() should throw an exception"
            + " when departureNumber is negative or zero")
    public void validateDepartureNumberShouldThrowExceptionWhenDepartureNumberIsNegative() {
      int departureNumber = -1;

      assertThrows(
          IllegalArgumentException.class,
          () -> ValidationUtils.validateDepartureNumber(departureNumber));
    }

    @Test
    @DisplayName("validateDepartureNumber() should throw an exception when departureNumber is zero")
    public void validateDepartureNumberShouldThrowExceptionWhenDepartureNumberIsZero() {
      int departureNumber = 0;

      assertThrows(
          IllegalArgumentException.class,
          () -> ValidationUtils.validateDepartureNumber(departureNumber));
    }

    @Test
    @DisplayName("validateLine() should throw an exception when line is empty")
    public void validateLineShouldThrowExceptionWhenLineIsEmpty() {
      String line = "";

      assertThrows(IllegalArgumentException.class, () -> ValidationUtils.validateLine(line));
    }

    @Test
    @DisplayName("validateDestination() should throw an exception when destination is empty")
    public void validateDestinationShouldThrowExceptionWhenDestinationIsEmpty() {
      String destination = "";

      assertThrows(
          IllegalArgumentException.class, () -> ValidationUtils.validateDestination(destination));
    }

    @Test
    @DisplayName("validateTime() should throw an exception when time is null")
    public void validateTimeShouldThrowExceptionWhenTimeIsNull() {
      Time time = new Time(LocalTime.parse("12:00"));
      ValidationUtils validationUtils = new ValidationUtils(time);

      assertThrows(NullPointerException.class, () -> validationUtils.validateTime(null));
    }

    @Test
    @DisplayName("validateDepartureTime() should throw an exception when departureTime is null")
    public void validateDepartureTimeShouldThrowExceptionWhenDepartureTimeIsNull() {
      Time time = new Time(LocalTime.parse("12:00"));
      ValidationUtils validationUtils = new ValidationUtils(time);

      assertThrows(NullPointerException.class, () -> validationUtils.validateDepartureTime(null));
    }

    @Test
    @DisplayName(
        "validateDepartureTime() should throw an exception when departureTime is before time")
    public void validateDepartureTimeShouldThrowExceptionWhenDepartureTimeIsBeforeTime() {
      Time time = new Time(LocalTime.parse("12:00"));
      ValidationUtils validationUtils = new ValidationUtils(time);

      assertThrows(
          IllegalArgumentException.class,
          () -> validationUtils.validateDepartureTime(LocalTime.parse("11:00")));
    }

    @Test
    @DisplayName("validateTrackNumber() should throw an exception when trackNumber is zero")
    public void validateTrackNumberShouldThrowExceptionWhenTrackNumberIsZero() {
      int trackNumber = 0;

      assertThrows(
          IllegalArgumentException.class, () -> ValidationUtils.validateTrackNumber(trackNumber));
    }

    @Test
    @DisplayName("validateTrainDeparture() should throw an exception when trainDeparture is null")
    public void validateTrainDepartureShouldThrowExceptionWhenTrainDepartureIsNull() {
      assertThrows(NullPointerException.class, () -> ValidationUtils.validateTrainDeparture(null));
    }

    @Test
    @DisplayName(
        "validateDepartureNumberUnique() should throw an"
            + " exception when departureNumber is not unique")
    public void validateDepartureNumberUniqueShouldThrowExceptionWhenDepartureNumberIsNotUnique() {
      LinkedList<TrainDeparture> trainDepartures = new LinkedList<>();
      ValidationUtils validationUtils = new ValidationUtils(new Time(LocalTime.parse("12:00")));
      trainDepartures.add(
          new TrainDeparture(1, "L1", "Trondheim", LocalTime.parse("12:00"), validationUtils));
      trainDepartures.add(
          new TrainDeparture(2, "L1", "Trondheim", LocalTime.parse("12:00"), validationUtils));
      trainDepartures.add(
          new TrainDeparture(3, "L1", "Trondheim", LocalTime.parse("12:00"), validationUtils));

      assertThrows(
          IllegalArgumentException.class,
          () -> ValidationUtils.validateDepartureNumberUnique(3, trainDepartures));
    }
  }
}
