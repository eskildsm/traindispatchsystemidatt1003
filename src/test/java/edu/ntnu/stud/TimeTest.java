package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class TimeTest {
  @Nested
  @DisplayName("Positive tests for Time methods")
  public class PositiveTests {
    Time time;

    @BeforeEach
    void init() {
      time = new Time(java.time.LocalTime.parse("00:00"));
    }

    @Test
    @DisplayName("Test constructor")
    public void testConstructor() {
      assertEquals(java.time.LocalTime.parse("00:00"), time.getCurrentTime());
    }

    @Test
    @DisplayName("Test setCurrentTime with valid parameter")
    public void testSetCurrentTimeWithValidParameter() {
      time.setCurrentTime(java.time.LocalTime.parse("01:00"));
      assertEquals(java.time.LocalTime.parse("01:00"), time.getCurrentTime());
    }
  }

  @Nested
  @DisplayName("Negative tests for Time methods")
  public class NegativeTests {
    @Test
    @DisplayName("Test constructor with null parameter")
    public void testConstructorWithNullParameter() {
      assertThrows(NullPointerException.class, () -> new Time(null));
    }

    @Test
    @DisplayName("Test setCurrentTime with null parameter")
    public void testSetCurrentTimeWithNullParameter() {
      Time time = new Time(java.time.LocalTime.parse("00:00"));
      assertThrows(NullPointerException.class, () -> time.setCurrentTime(null));
    }
  }
}
