package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class TrainDepartureRegisterTest {
  @Nested
  @DisplayName("Positive tests for TrainDepartureRegister methods")
  public class PositiveTests {
    TrainDepartureRegister trainDepartureRegister;

    @BeforeEach
    public void init() {
      trainDepartureRegister = new TrainDepartureRegister(LocalTime.parse("00:00"));
    }

    @Test
    @DisplayName("Test constructor")
    public void testConstructor() {
      assertEquals(0, trainDepartureRegister.getTrainDepartures().size());
    }

    @Test
    @DisplayName("Test addTrainDeparture with valid parameter and one train departure")
    public void testAddTrainDepartureWithOneTrainDeparture() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("11:00"));
      assertEquals(
          "Departure Time:11:00, Line:'line', Train Departure Number:1,"
              + " Destination:'destination', Delay:00:00, Track:unknown",
          trainDepartureRegister.toString());
    }

    @Test
    @DisplayName("Test addTrainDeparture with valid parameter")
    public void testAddTrainDepartureWithTwoTrainDeparture() {
      trainDepartureRegister.createTrainDeparture(
          1, "L1", "Kongsberg", java.time.LocalTime.parse("12:00"));
      trainDepartureRegister.createTrainDeparture(
          2, "L2", "Oslo", java.time.LocalTime.parse("11:00"));
      assertEquals(
          "Departure Time:11:00, Line:'L2', Train Departure Number:2,"
              + " Destination:'Oslo', Delay:00:00, Track:unknown"
              + "\nDeparture Time:12:00, Line:'L1', Train Departure Number:1,"
              + " Destination:'Kongsberg', Delay:00:00, Track:unknown",
          trainDepartureRegister.toString());
    }

    @Test
    @DisplayName("Test setTrackForDeparture to 1")
    public void testSetTrackForDepartureTo1() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.setTrackForDeparture(1, 1);
      assertEquals(
          "Departure Time:11:00, Line:'line', Train Departure Number:1,"
              + " Destination:'destination', Delay:00:00, Track:1",
          trainDepartureRegister.toString());
    }

    @Test
    @DisplayName("Test setTrackForDeparture to unknown")
    public void testSetTrackForDepartureToUnknown() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.setTrackForDeparture(1, 1);
      trainDepartureRegister.setTrackForDeparture(1, 0);
      assertEquals(
          "Departure Time:11:00, Line:'line', Train Departure Number:1,"
              + " Destination:'destination', Delay:00:00, Track:unknown",
          trainDepartureRegister.toString());
    }

    @Test
    @DisplayName("Test setDelay to 00:10")
    public void testSetDelayTo00_10() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.setDelayForDeparture(1, java.time.LocalTime.parse("00:10"));
      assertEquals(
          "Departure Time:11:00, Line:'line', Train Departure Number:1,"
              + " Destination:'destination', Delay:00:10, Track:unknown",
          trainDepartureRegister.toString());
    }

    @Test
    @DisplayName("Test search for train departure with train departure number as valid parameter")
    public void testSearchForTrainDepartureWithTrainDepartureNumberAsValidParameter() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.createTrainDeparture(
          2, "line", "destination", java.time.LocalTime.parse("10:00"));

      assertEquals(
          "Departure Time:11:00, Line:'line', Train Departure Number:1,"
              + " Destination:'destination', Delay:00:00, Track:unknown",
          trainDepartureRegister.searchForTrainDepartureByDepartureNumber(1));
    }

    @Test
    @DisplayName("Test search for train departure with line as valid parameter")
    public void testSearchForTrainDepartureWithLineAsValidParameter() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "Trondheim", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.createTrainDeparture(
          2, "line", "Trondheim", java.time.LocalTime.parse("10:00"));
      trainDepartureRegister.createTrainDeparture(
          3, "line2", "Oslo", java.time.LocalTime.parse("10:00"));

      assertEquals(
          "Departure Time:10:00, Line:'line', Train Departure Number:2,"
              + " Destination:'Trondheim', Delay:00:00, Track:unknown\n"
              + "Departure Time:11:00, Line:'line', Train Departure Number:1,"
              + " Destination:'Trondheim', Delay:00:00, Track:unknown",
          trainDepartureRegister.searchForTrainDepartureByLine("line"));
    }

    @Test
    @DisplayName("Test search for train departure with track as valid parameter")
    public void testSearchForTrainDepartureWithTrackAsValidParameter() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "Trondheim", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.createTrainDeparture(
          2, "line", "Trondheim", java.time.LocalTime.parse("10:00"));
      trainDepartureRegister.createTrainDeparture(
          3, "line2", "Oslo", java.time.LocalTime.parse("10:00"));
      trainDepartureRegister.setTrackForDeparture(1, 1);
      trainDepartureRegister.setTrackForDeparture(2, 1);

      assertEquals(
          "Departure Time:10:00, Line:'line', Train Departure Number:2,"
              + " Destination:'Trondheim', Delay:00:00, Track:1\n"
              + "Departure Time:11:00, Line:'line', Train Departure Number:1,"
              + " Destination:'Trondheim', Delay:00:00, Track:1",
          trainDepartureRegister.searchForTrainDepartureByTrack(1));
    }

    @Test
    @DisplayName("Test search for train departure with destination as valid parameter")
    public void testSearchForTrainDepartureWithDestinationAsValidParameter() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "Trondheim", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.createTrainDeparture(
          2, "line", "Trondheim", java.time.LocalTime.parse("10:00"));
      trainDepartureRegister.createTrainDeparture(
          3, "line", "Oslo", java.time.LocalTime.parse("10:00"));

      assertEquals(
          "Departure Time:10:00, Line:'line', Train Departure Number:2,"
              + " Destination:'Trondheim', Delay:00:00, Track:unknown\n"
              + "Departure Time:11:00, Line:'line', Train Departure Number:1,"
              + " Destination:'Trondheim', Delay:00:00, Track:unknown",
          trainDepartureRegister.searchForTrainDepartureByDestination("Trondheim"));
    }

    @Test
    @DisplayName("Test removeTrainDeparture with valid parameter")
    public void testRemoveTrainDeparture() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.createTrainDeparture(
          2, "line", "destination", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.removeTrainDeparture(1);

      assertEquals(
          "Departure Time:11:00, Line:'line', Train Departure Number:2,"
              + " Destination:'destination', Delay:00:00, Track:unknown",
          trainDepartureRegister.toString());
    }

    @Test
    @DisplayName("Test timeUpdate with valid parameter")
    public void testTimeUpdateRemoveDepartedTrainDeparture() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("11:00"));

      trainDepartureRegister.setCurrentTime(java.time.LocalTime.parse("12:00"));

      assertEquals(0, trainDepartureRegister.getTrainDepartures().size());
    }

    @Test
    @DisplayName("Test timeUpdate with delayed train departure, not departed")
    public void testTimeUpdateWithDelayedTrainDepartureNotDeparted() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.setDelayForDeparture(1, java.time.LocalTime.parse("00:10"));

      trainDepartureRegister.setCurrentTime(java.time.LocalTime.parse("11:10"));

      assertEquals(1, trainDepartureRegister.getTrainDepartures().size());
    }

    @Test
    @DisplayName(
        "Test checkDepartureNumberUnique does not throw exception, with unique departure number")
    public void testCheckDepartureNumberUniqueWithUniqueDepartureNumber() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("11:00"));
      trainDepartureRegister.checkDepartureNumberUnique(2);
    }

    @Test
    @DisplayName("Test checkDestination does not throw exception, with valid destination")
    public void testCheckDestinationWithValidDestination() {
      trainDepartureRegister.checkDestination("Oslo");
    }

    @Test
    @DisplayName("Test checkLine does not throw exception, with valid line")
    public void testCheckLineWithValidLine() {
      trainDepartureRegister.checkLine("line");
    }

    @Test
    @DisplayName("Test checkTrack does not throw exception, with valid track")
    public void testCheckTrackWithValidTrack() {
      trainDepartureRegister.checkTrack(1);
    }

    @Test
    @DisplayName("Test checkDelay with valid parameter")
    public void testSetDelayWithValidParameter() {
      trainDepartureRegister.checkDelay(java.time.LocalTime.parse("00:10"));
    }

    @Test
    @DisplayName("Test checkDepartureTime does not throw exception, with valid departure time")
    public void testCheckDepartureTimeWithValidDepartureTime() {
      trainDepartureRegister.checkDepartureTime(java.time.LocalTime.parse("11:00"));
    }

    @Test
    @DisplayName("Test toString")
    public void testToString() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("12:00"));
      trainDepartureRegister.createTrainDeparture(
          2, "line2", "destination2", java.time.LocalTime.parse("11:00"));
      assertEquals(
          "Departure Time:11:00, Line:'line2', Train Departure Number:2,"
              + " Destination:'destination2', Delay:00:00, Track:unknown\n"
              + "Departure Time:12:00, Line:'line', Train Departure Number:1,"
              + " Destination:'destination', Delay:00:00, Track:unknown",
          trainDepartureRegister.toString());
    }
  }

  @Nested
  @DisplayName("Negative tests for TrainDepartureRegister methods")
  public class NegativeTests {
    TrainDepartureRegister trainDepartureRegister;

    @BeforeEach
    public void init() {
      trainDepartureRegister = new TrainDepartureRegister(LocalTime.parse("12:00"));
    }

    @Test
    @DisplayName("Test addTrainDeparture with duplicate train departure number")
    public void testAddTrainDepartureWithDuplicateTrainDepartureNumber() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("12:00"));
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.createTrainDeparture(
              1, "line", "destination", LocalTime.parse("12:00")));
    }

    @Test
    @DisplayName("Test addTrainDeparture with departure time before current time")
    public void testAddTrainDepartureWithDepartureTimeBeforeCurrentTime() {
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.createTrainDeparture(
              1, "line", "destination", LocalTime.parse("11:00")));
    }

    @Test
    @DisplayName("Test setDelay with null parameter")
    public void testSetDelayWithNullParameter() {
      assertThrows(
          NullPointerException.class,
          () -> trainDepartureRegister.setDelayForDeparture(1, null));
    }

    @Test
    @DisplayName("Test search for train departure with train departure number as invalid parameter")
    public void testSearchForTrainDepartureWithTrainDepartureNumberAsInvalidParameter() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("12:00"));
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.searchForTrainDepartureByDepartureNumber(2));
    }

    @Test
    @DisplayName("Test search for train departure with destination as invalid parameter")
    public void testSearchForTrainDepartureWithDestinationAsInvalidParameter() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("12:00"));
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.searchForTrainDepartureByDestination("Oslo"));
    }

    @Test
    @DisplayName("Test removeDepartedTrainDepartures with null parameter")
    public void testRemoveDepartedTrainDeparturesWithNullParameter() {
      assertThrows(
          NullPointerException.class,
          () -> trainDepartureRegister.setCurrentTime(null));
    }

    @Test
    @DisplayName("Test timeUpdate with delayed train departure, but departed")
    public void testTimeUpdateWithDelayedTrainDepartureDeparted() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("12:00"));
      trainDepartureRegister.setDelayForDeparture(1, java.time.LocalTime.parse("00:09"));

      trainDepartureRegister.setCurrentTime(java.time.LocalTime.parse("12:10"));

      assertEquals(0, trainDepartureRegister.getTrainDepartures().size());
    }

    @Test
    @DisplayName(
        "Test checkDepartureNumberUnique throws exception, with duplicate departure number")
    public void testCheckDepartureNumberUniqueWithDuplicateDepartureNumber() {
      trainDepartureRegister.createTrainDeparture(
          1, "line", "destination", java.time.LocalTime.parse("12:00"));
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.checkDepartureNumberUnique(1));
    }

    @Test
    @DisplayName("Test checkDestination throws exception, with invalid destination")
    public void testCheckDestinationWithInvalidDestination() {
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.checkDestination(""));
    }

    @Test
    @DisplayName("Test checkLine throws exception, with invalid line")
    public void testCheckLineWithInvalidLine() {
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.checkLine(""));
    }

    @Test
    @DisplayName("Test checkTrack throws exception, with invalid track")
    public void testCheckTrackWithInvalidTrack() {
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.checkTrack(0));
    }

    @Test
    @DisplayName("Test checkDelay throws exception, with null as delay")
    public void testCheckDelayWithInvalidDelay() {
      assertThrows(
          NullPointerException.class,
          () -> trainDepartureRegister.checkDelay(null));
    }

    @Test
    @DisplayName("Test checkDepartureTime throws exception, with invalid departure time")
    public void testCheckDepartureTimeWithInvalidDepartureTime() {
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.checkDepartureTime(LocalTime.parse("11:59")));
    }
  }
}
