package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.LinkedList;

/**
 * Validation utils class for the train dispatch application. It validates the input
 * from the user.
 *
 * @author 10031
 * @version 1.0
 */
public class ValidationUtils {
  private final Time time;

  /**
   * Constructor to create a new instance of ValidationUtils.
   *
   * @param time - instance of Time
   */
  public ValidationUtils(Time time) {
    this.time = time;
  }

  /**
   * Validate the departure number.
   *
   * @param departureNumber - the departure number to be validated
   * @throws IllegalArgumentException if departureNumber is negative or zero
   */
  public static void validateDepartureNumber(int departureNumber) throws IllegalArgumentException {
    if (departureNumber <= 0) {
      throw new IllegalArgumentException("Departure number must be positive and non-zero");
    }
  }

  /**
   * Validate the line.
   *
   * @param line - the line to be validated
   * @throws IllegalArgumentException if line is empty
   */
  public static void validateLine(String line) throws IllegalArgumentException {
    if (line.isEmpty()) {
      throw new IllegalArgumentException("Line must be set");
    }
  }

  /**
   * Validate the destination.
   *
   * @param destination - the destination to be validated
   * @throws IllegalArgumentException if destination is empty
   */
  public static void validateDestination(String destination) throws IllegalArgumentException {
    if (destination.isEmpty()) {
      throw new IllegalArgumentException("Destination must be set");
    }
  }

  /**
   * Validate the time.
   *
   * @param time - the time to be validated
   * @throws NullPointerException if time is null
   */
  public void validateTime(LocalTime time) throws NullPointerException {
    if (time == null) {
      throw new NullPointerException("Time must not be null");
    }
  }

  /**
   * Validate the departure time.
   *
   * @param departureTime - the departure time to be validated
   * @throws IllegalArgumentException if departure time is before current time
   * @throws NullPointerException if departureTime is null
   */
  public void validateDepartureTime(LocalTime departureTime) throws IllegalArgumentException {
    validateTime(departureTime);
    if (departureTime.isBefore(time.getCurrentTime())) {
      throw new IllegalArgumentException("Departure time cannot be before current time");
    }
  }

  /**
   * Validate the track number.
   *
   * @param trackNumber - the track number to be validated
   * @throws IllegalArgumentException if trackNumber is negative or zero and is not -1
   */
  public static void validateTrackNumber(int trackNumber) throws IllegalArgumentException {
    if (trackNumber <= 0 && trackNumber != -1) {
      throw new IllegalArgumentException("Track number must be positive and non-zero or -1");
    }
  }

  /**
   * Validate TrainDeparture object is not null.
   *
   * @param trainDeparture - the TrainDeparture object to be validated
   * @throws NullPointerException if trainDeparture is null
   */
  public static void validateTrainDeparture(TrainDeparture trainDeparture)
      throws NullPointerException {
    if (trainDeparture == null) {
      throw new NullPointerException("Train departure must not be null");
    }
  }

  /**
   * This method is used to validate the departure number is unique.
   *
   * @param departureNumber - the departure number to be validated
   * @throws IllegalArgumentException if departure number already exists
   */
  public static void validateDepartureNumberUnique(
      int departureNumber, LinkedList<TrainDeparture> trainDepartures)
      throws IllegalArgumentException {
    if (trainDepartures.stream()
        .anyMatch(trainDeparture -> trainDeparture.getTrainDepartureNumber() == departureNumber)) {
      throw new IllegalArgumentException("Train departure number already exists");
    }
  }
}
