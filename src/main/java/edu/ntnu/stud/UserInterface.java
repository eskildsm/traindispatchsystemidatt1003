package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * User interface class for the train dispatch application. It runs menus, handle the user input and
 * display data to the user.
 *
 * @author 10031
 * @version 1.0
 */
public class UserInterface {
  private final TrainDepartureRegister trainDepartureRegister;
  private final UserInput userInput;

  /** Constructor for the user interface class. */
  public UserInterface() {
    this.trainDepartureRegister = new TrainDepartureRegister(LocalTime.parse("00:00"));
    this.userInput = new UserInput(trainDepartureRegister);
  }

  /** Init test data used in current version of user interface class. */
  public void init() {
    trainDepartureRegister.createTrainDeparture(1, "L1", "Oslo", LocalTime.parse("12:00"));
    trainDepartureRegister.setTrackForDeparture(1, 1);
    trainDepartureRegister.createTrainDeparture(2, "L2", "Drammen", LocalTime.parse("12:10"));
    trainDepartureRegister.createTrainDeparture(3, "L3", "Trondheim", LocalTime.parse("10:00"));
    trainDepartureRegister.createTrainDeparture(4, "L3", "Trondheim", LocalTime.parse("10:10"));
  }

  /** Add a train departure to the train departure register. */
  private void addTrainDeparture() {
    System.out.println("Crate new train departure: ");

    int departureNumber = this.userInput.getUniqueDepartureNumber();
    String line = this.userInput.getValidLine();
    String destination = this.userInput.getValidDestination();
    LocalTime departureTime = this.userInput.getValidDepartureTime();

    try {
      trainDepartureRegister.createTrainDeparture(
          departureNumber, line, destination, departureTime);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    int track = this.userInput.getValidTrack();
    try {
      trainDepartureRegister.setTrackForDeparture(departureNumber, track);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /** Delete a train departure with the given departure number. */
  private void deleteTrainDeparture() {
    int departureNumber = this.userInput.getExistingDepartureNumber();

    try {
      this.trainDepartureRegister.removeTrainDeparture(departureNumber);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    addSpace();
  }

  /** Set track number of train departure. */
  private void setTrackNumber() {
    int departureNumber = this.userInput.getExistingDepartureNumber();
    int track = this.userInput.getValidTrack();

    try {
      this.trainDepartureRegister.setTrackForDeparture(departureNumber, track);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    addSpace();
  }

  /** Set current time. */
  private void setTime() {
    LocalTime time = this.userInput.getValidTime("current time");

    try {
      this.trainDepartureRegister.setCurrentTime(time);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    addSpace();
  }

  /** Set delay of train departure. */
  private void setDelay() {
    int departureNumber = this.userInput.getExistingDepartureNumber();
    LocalTime delay = this.userInput.getValidTime("delay");

    try {
      this.trainDepartureRegister.setDelayForDeparture(departureNumber, delay);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    addSpace();
  }

  /** Write the train departure with the given departure number to the console. */
  private void getTrainDepartureByDepartureNumber() {
    int departureNumber = this.userInput.getExistingDepartureNumber();
    try {
      System.out.println(
          this.trainDepartureRegister.searchForTrainDepartureByDepartureNumber(departureNumber));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    addSpace();
  }

  /** Write the all train departures with the given destination to the console. */
  private void getTrainDeparturesByDestination() {
    System.out.println("Enter destination: ");
    String destination = this.userInput.getStringInput();

    try {
      System.out.println(
          this.trainDepartureRegister.searchForTrainDepartureByDestination(destination));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    addSpace();
  }

  /** Search for train departures with the given track number. */
  private void getTrainDeparturesByTrackNumber() {
    System.out.println("Enter track number: ");
    int trackNumber = this.userInput.getIntInput();

    try {
      System.out.println(this.trainDepartureRegister.searchForTrainDepartureByTrack(trackNumber));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    addSpace();
  }

  /** Search for train departures with the given line number. */
  private void getTrainDeparturesByLineNumber() {
    System.out.println("Enter line number: ");
    String lineNumber = this.userInput.getStringInput();

    try {
      System.out.println(this.trainDepartureRegister.searchForTrainDepartureByLine(lineNumber));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    addSpace();
  }

  /** Write all train departures to the console. */
  private void getAllTrainDepartures() {
    System.out.println(this.trainDepartureRegister);
    addSpace();
  }

  /**
   * Activate submenu for updating the values for the train departure with the given departure
   * number.
   */
  private void updateValuesSubMenu() {
    showUpdateSubMenu();
    int choice = this.userInput.getChoice();
    addSpace();
    executeChoice(choice, 1);
  }

  /** Activate submenu for searching for train departures. */
  private void searchSubMenu() {
    showSearchSubMenu();
    int choice = this.userInput.getChoice();
    addSpace();
    executeChoice(choice, 2);
  }

  /**
   * execute user choice.
   *
   * @param choice user choice
   * @param menu menuIndex
   */
  private void executeChoice(int choice, int menu) {
    if (menu == 0) {
      switch (choice) {
        case 1 -> addTrainDeparture();
        case 2 -> deleteTrainDeparture();
        case 3 -> updateValuesSubMenu();
        case 4 -> getAllTrainDepartures();
        case 5 -> searchSubMenu();
        case 6 -> setTime();
        case 7 -> System.exit(0);
        default -> System.out.println("Invalid input");
      }
    } else if (menu == 1) {
      switch (choice) {
        case 1 -> setTrackNumber();
        case 2 -> setDelay();
        case 3 -> {
          break;
        }
        default -> System.out.println("Invalid input");
      }
    } else {
      switch (choice) {
        case 1 -> getTrainDepartureByDepartureNumber();
        case 2 -> getTrainDeparturesByDestination();
        case 3 -> getTrainDeparturesByTrackNumber();
        case 4 -> getTrainDeparturesByLineNumber();
        case 5 -> {
          break;
        }
        default -> System.out.println("Invalid input");
      }
    }
  }

  /** Obtain space to console. */
  private void addSpace() {
    System.out.println();
  }

  /** Write main menu to console. */
  private void showMenu() {
    System.out.println("1. Add train departure");
    System.out.println("2. Delete train departure");
    System.out.println("3. Update values for train departure");
    System.out.println("4. Get all train departures");
    System.out.println("5. Search for train departure");
    System.out.println("6. Update time of day");
    System.out.println("7. Exit");
  }

  /** Write update values submenu to console. */
  private void showUpdateSubMenu() {
    System.out.println("1. Update track number");
    System.out.println("2. Update delay");
    System.out.println("3. Back");
  }

  /** Write search submenu to console. */
  private void showSearchSubMenu() {
    System.out.println("1. Search by departure number");
    System.out.println("2. Search by destination");
    System.out.println("3. Search by track number");
    System.out.println("4. Search by line number");
    System.out.println("5. Back");
  }

  /** Write welcome message to console. */
  public void welcomeMessage() {
    System.out.println("Welcome to the train dispatch application!");
    System.out.println("==========================================");
    System.out.println();

    System.out.println("Instructions:");
    System.out.println("To chose an option, enter the number before the option and press enter.");
    System.out.println(
        "If option requires input , "
            + "type in the input in accordance to instructions above input field and press enter.");
    System.out.println();

    System.out.println("==========================================");
    System.out.println();
    System.out.println("Some test data has been added to the application:");
    getAllTrainDepartures();
  }

  /** Start the user interface. */
  public void start() {
    welcomeMessage();

    while (true) {
      showMenu();
      int choice = this.userInput.getChoice();
      addSpace();
      executeChoice(choice, 0);
    }
  }
}
