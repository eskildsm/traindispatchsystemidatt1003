package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Register for train departures. It is used to store train departures and
 * to update values in the train departures. It is also used to get information about the train
 * departures.
 *
 * @author 10031
 * @version 1.0
 */
public class TrainDepartureRegister {
  private final LinkedList<TrainDeparture> trainDepartures;
  private final Time time;
  private final ValidationUtils validationUtils;

  /**
   * Constructor to create a new instance of TrainDepartureRegister.
   *
   * @param currentTime - the current time
   * @throws NullPointerException if currentTime is null
   */
  public TrainDepartureRegister(LocalTime currentTime) {
    trainDepartures = new LinkedList<>();
    time = new Time(currentTime);
    validationUtils = new ValidationUtils(time);
  }

  /**
   * Add a train departure to the register.
   *
   * @param trainDeparture the train departure to be added
   * @throws NullPointerException if trainDeparture is null
   */
  private void addTrainDeparture(TrainDeparture trainDeparture) throws NullPointerException {
    ValidationUtils.validateTrainDeparture(trainDeparture);

    boolean added = false;

    ListIterator<TrainDeparture> iterator = trainDepartures.listIterator(0);
    while (iterator.hasNext()) {
      TrainDeparture registeredTrainDeparture = iterator.next();
      if (trainDeparture.getDepartureTime().isBefore(registeredTrainDeparture.getDepartureTime())) {
        iterator.previous();
        iterator.add(trainDeparture);
        added = true;
        return;
      }
    }

    if (!added) {
      trainDepartures.add(trainDeparture);
    }
  }

  /**
   * Create and add TrainDeparture to the register.
   *
   * @param trainDepartureNumber - the number of the train departure
   * @param line - the line of the train departure
   * @param destination - the destination of the train departure
   * @throws IllegalArgumentException if trainDepartureNumber already exists or is negative or zero
   * @throws IllegalArgumentException if line is empty
   * @throws IllegalArgumentException if destination is empty
   * @throws IllegalArgumentException if departure time is before current time or null
   */
  public void createTrainDeparture(
      int trainDepartureNumber, String line, String destination, LocalTime departureTime)
      throws IllegalArgumentException {
    ValidationUtils.validateDepartureNumberUnique(trainDepartureNumber, trainDepartures);

    TrainDeparture trainDeparture =
        new TrainDeparture(trainDepartureNumber, line, destination, departureTime, validationUtils);
    addTrainDeparture(trainDeparture);
  }

  /**
   * Set the track number of a train departure.
   *
   * @param trainDepartureNumber - the number of the train departure
   * @param track - the track number of the train departure
   * @throws IllegalArgumentException if trainDepartureNumber does not correspond to any train
   *     departure
   */
  public void setTrackForDeparture(int trainDepartureNumber, int track)
      throws IllegalArgumentException {
    Predicate<TrainDeparture> predicate =
        (trainDepartureObj) -> trainDepartureObj.getTrainDepartureNumber() == trainDepartureNumber;

    TrainDeparture trainDeparture = searchForTrainDeparture(predicate, false).getFirst();

    if (track == 0) {
      trainDeparture.setTrack(-1);
      return;
    }

    trainDeparture.setTrack(track);
  }

  /**
   * Set delay of a train departure.
   *
   * @param trainDepartureNumber - the number of the train departure
   * @param delay - the delay of the train departure
   * @throws IllegalArgumentException if trainDepartureNumber does not correspond to any train
   * @throws NullPointerException if delay is null
   */
  public void setDelayForDeparture(int trainDepartureNumber, LocalTime delay)
      throws IllegalArgumentException, NullPointerException {
    validationUtils.validateTime(delay);

    Predicate<TrainDeparture> predicate =
        (trainDepartureObj) -> trainDepartureObj.getTrainDepartureNumber() == trainDepartureNumber;

    TrainDeparture trainDeparture = searchForTrainDeparture(predicate, false).getFirst();
    trainDeparture.setDelay(delay);
  }

  /**
   * Search for train departures.
   *
   * @param predicate - the predicate used to search for train departures
   * @param multiple - if multiple results should be returned
   * @return the TrainDeparture object with the corresponding predicate
   * @throws IllegalArgumentException if predicate does not correspond to any train
   */
  private LinkedList<TrainDeparture> searchForTrainDeparture(
      Predicate<TrainDeparture> predicate, boolean multiple) throws IllegalArgumentException {

    LinkedList<TrainDeparture> resultDepartures = new LinkedList<>();
    Stream<TrainDeparture> resultDeparture = trainDepartures.stream();

    if (!multiple) {
      Optional<TrainDeparture> firstMatch = resultDeparture.filter(predicate).findFirst();

      if (firstMatch.isPresent()) {
        resultDepartures.add(firstMatch.get());
      } else {
        throw new IllegalArgumentException("No results found in register");
      }
    } else {
      resultDepartures.addAll(resultDeparture.filter(predicate).toList());
    }

    if (resultDepartures.isEmpty()) {
      throw new IllegalArgumentException("No results found in register");
    }

    return resultDepartures;
  }

  /**
   * Search for train departure string by train departure number.
   *
   * @param trainDepartureNumber - the number of the train departure
   * @return a string with the train departure with the corresponding train departure number
   * @throws IllegalArgumentException if trainDepartureNumber does not correspond to any train
   */
  public String searchForTrainDepartureByDepartureNumber(int trainDepartureNumber)
      throws IllegalArgumentException {
    Predicate<TrainDeparture> predicate =
        (trainDepartureObj) -> trainDepartureObj.getTrainDepartureNumber() == trainDepartureNumber;

    return searchForTrainDeparture(predicate, false).getFirst().toString();
  }

  /**
   * Search for train departure string by line.
   *
   * @param line - the line of the train departure
   * @return a string with all the train departures with the corresponding line
   * @throws IllegalArgumentException if line does not correspond to any train departure
   */
  public String searchForTrainDepartureByLine(String line) throws IllegalArgumentException {
    Predicate<TrainDeparture> predicate =
        (trainDepartureObj) -> trainDepartureObj.getLine().equals(line);

    LinkedList<TrainDeparture> trainDeparture = searchForTrainDeparture(predicate, true);

    return selectedTrainDepartureString(trainDeparture);
  }

  /**
   * Search for train departure string by track.
   *
   * @param track - the track of the train departure
   * @return a string with all the train departures with the corresponding track
   * @throws IllegalArgumentException if track does not correspond to any train departure
   */
  public String searchForTrainDepartureByTrack(int track) throws IllegalArgumentException {
    Predicate<TrainDeparture> predicate =
        (trainDepartureObj) -> trainDepartureObj.getTrack() == track;

    LinkedList<TrainDeparture> trainDeparture = searchForTrainDeparture(predicate, true);

    return selectedTrainDepartureString(trainDeparture);
  }

  /**
   * Search for train departures strings by destination.
   *
   * @param destination - the destination of the train departure
   * @return a string with all the train departures with the corresponding destination
   * @throws IllegalArgumentException if destination does not correspond to any train departure
   */
  public String searchForTrainDepartureByDestination(String destination)
      throws IllegalArgumentException {
    Predicate<TrainDeparture> predicate =
        (trainDepartureObj) -> trainDepartureObj.getDestination().equals(destination);

    LinkedList<TrainDeparture> trainDeparture = searchForTrainDeparture(predicate, true);

    return selectedTrainDepartureString(trainDeparture);
  }

  /**
   * Remove TrainDeparture from the register.
   *
   * @param trainDepartureNumber - the number of the train departure
   * @throws IllegalArgumentException if trainDepartureNumber does not correspond to any train
   */
  public void removeTrainDeparture(int trainDepartureNumber) throws IllegalArgumentException {
    Predicate<TrainDeparture> predicate =
        (trainDepartureObj) -> trainDepartureObj.getTrainDepartureNumber() == trainDepartureNumber;

    TrainDeparture trainDeparture = searchForTrainDeparture(predicate, false).getFirst();

    trainDepartures.remove(trainDeparture);
  }

  /**
   * Remove departed train departures from the register.
   *
   * @param currentTime - the current time
   * @throws NullPointerException if currentTime is null
   */
  private void removeDepartedTrainDepartures(LocalTime currentTime) throws NullPointerException {
    validationUtils.validateTime(currentTime);

    ListIterator<TrainDeparture> iterator = trainDepartures.listIterator(0);
    while (iterator.hasNext()) {
      TrainDeparture trainDeparture = iterator.next();
      if (trainDeparture
          .getDepartureTime()
          .isBefore(currentTime)) { // Check if departure time is before current time
        if ((trainDeparture
                .getDepartureTime()
                .plusHours(trainDeparture.getDelay().getHour())
                .plusMinutes(trainDeparture.getDelay().getMinute()))
            .isBefore(currentTime)) {
          iterator.remove(); // Check if departure time and delay is before current time
        }
      } else { // Linked list is sorted by departure time, so if departure time is not before
        // current time, no more departures are before current time
        break;
      }
    }
  }

  /**
   * Update the time of the register.
   *
   * @param currentTime - the current time
   * @throws NullPointerException if currentTime is null
   */
  public void setCurrentTime(LocalTime currentTime) throws NullPointerException {
    time.setCurrentTime(currentTime);
    removeDepartedTrainDepartures(currentTime);
  }

  /**
   * Obtain all train departures.
   *
   * @return a ArrayList with all the train departures as Strings
   */
  public ArrayList<String> getTrainDepartures() {
    return trainDepartures.stream()
        .map(Object::toString)
        .collect(Collectors.toCollection(ArrayList::new));
  }

  /**
   * Check if departure number is valid and unique.
   *
   * @param trainDepartureNumber - the number of the train departure
   * @throws IllegalArgumentException if trainDepartureNumber already exists or is negative or zero
   */
  public void checkDepartureNumberUnique(int trainDepartureNumber) throws IllegalArgumentException {
    ValidationUtils.validateDepartureNumberUnique(trainDepartureNumber, trainDepartures);
    ValidationUtils.validateDepartureNumber(trainDepartureNumber);
  }

  /**
   * Check that destination is valid.
   *
   * @param destination - the destination of the train departure
   * @throws IllegalArgumentException if destination is empty
   */
  public static void checkDestination(String destination) throws IllegalArgumentException {
    ValidationUtils.validateDestination(destination);
  }

  /**
   * Check that line is valid.
   *
   * @param line - the line of the train departure
   * @throws IllegalArgumentException if line is empty
   */
  public static void checkLine(String line) throws IllegalArgumentException {
    ValidationUtils.validateLine(line);
  }

  /**
   * Check that track is valid.
   *
   * @param track - the track of the train departure
   * @throws IllegalArgumentException if track is negative or zero
   */
  public static void checkTrack(int track) throws IllegalArgumentException {
    ValidationUtils.validateTrackNumber(track);
  }

  /**
   * Check that delay is valid.
   *
   * @param delay - the delay of the train departure
   * @throws IllegalArgumentException if delay is null
   */
  public void checkDelay(LocalTime delay) throws IllegalArgumentException {
    validationUtils.validateTime(delay);
  }

  /**
   * Check that departure time is valid.
   *
   * @param departureTime - the departure time of the train departure
   * @throws IllegalArgumentException if departure time is before current time or null
   */
  public void checkDepartureTime(LocalTime departureTime) throws IllegalArgumentException {
    validationUtils.validateDepartureTime(departureTime);
  }

  /**
   * Obtain all train departures in linked list as String.
   *
   * @param selectTrainDepartures - the linked list of train departures
   * @return a string with all the train departures
   */
  private static String selectedTrainDepartureString(
      LinkedList<TrainDeparture> selectTrainDepartures) {
    return selectTrainDepartures.stream().map(Object::toString).collect(Collectors.joining("\n"));
  }

  /**
   * Obtain all train departures as string.
   *
   * @return a String with all the train departures
   */
  public String toString() {
    return selectedTrainDepartureString(trainDepartures);
  }
}
