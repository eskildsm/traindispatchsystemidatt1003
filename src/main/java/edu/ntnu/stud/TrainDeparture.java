package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * Store data regarding a train departure It is used by another class to keep track over: train
 * departures. trainDepartureNumber, departureTime, line, destination variables are final because
 * values should not be changed after the object is created.
 *
 * @author 10031
 * @version 1.0
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final int trainDepartureNumber;
  private final String destination;
  private int track;
  private LocalTime delay;
  private final ValidationUtils validationUtils;

  /**
   * Constructor to create a new instance of TrainDeparture, with trainDepartureNumber as parameter.
   *
   * @param trainDepartureNumber the number of the train departure
   * @throws IllegalArgumentException if trainDepartureNumber is negative or zero, or if line or
   *     destination is empty
   * @throws NullPointerException if departureTime is null
   * @throws NullPointerException if validationUtils is null
   */
  public TrainDeparture(
      int trainDepartureNumber,
      String line,
      String destination,
      LocalTime departureTime,
      ValidationUtils validationUtils)
      throws IllegalArgumentException, NullPointerException {
    if (validationUtils == null) {
      throw new NullPointerException("ValidationUtils must not be null");
    }

    this.validationUtils = validationUtils;

    ValidationUtils.validateDepartureNumber(trainDepartureNumber);
    ValidationUtils.validateLine(line);
    ValidationUtils.validateDestination(destination);
    validationUtils.validateDepartureTime(departureTime);

    setTrack(-1);
    this.line = line;
    this.destination = destination;
    setDelay(LocalTime.of(0, 0));
    this.trainDepartureNumber = trainDepartureNumber;
    this.departureTime = departureTime;
  }

  /**
   * Set the track of the train departure.
   *
   * @param track the track number of the train departure
   * @throws IllegalArgumentException if track is negative or zero
   */
  public void setTrack(int track) throws IllegalArgumentException {
    ValidationUtils.validateTrackNumber(track);
    this.track = track;
  }

  /**
   * Set the delay of the train.
   *
   * @param delay the delay of the train, default is 00:00
   * @throws NullPointerException if delay is null
   */
  public void setDelay(LocalTime delay) throws NullPointerException {
    validationUtils.validateTime(delay);

    this.delay = delay;
  }

  /**
   * Obtain the departure time of the train.
   *
   * @return the departure time of the train as a LocalTime object
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Obtain the line of the train.
   *
   * @return the line of the train as a String
   */
  public String getLine() {
    return line;
  }

  /**
   * Obtain the train departure number.
   *
   * @return the train departure number as an int
   */
  public int getTrainDepartureNumber() {
    return trainDepartureNumber;
  }

  /**
   * Obtain the destination of the train.
   *
   * @return the destination of the train as a String
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Obtain the track of the train departure.
   *
   * @return the track of the train departure as an int
   */
  public int getTrack() {
    return track;
  }

  /**
   * Obtain the delay of the train.
   *
   * @return the delay of the train as a LocalTime object
   */
  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Obtain the values of the train departure as String.
   *
   * @return a string containing the values of the train departure and values will be "unknown" if
   *     not set
   */
  @Override
  public String toString() {
    return "Departure Time:"
        + (this.departureTime == null ? "unknown" : this.departureTime.toString())
        + ", Line:'"
        + (this.line == null ? "unknown" : this.line)
        + '\''
        + ", Train Departure Number:"
        + (this.trainDepartureNumber == -1 ? "unknown" : this.trainDepartureNumber)
        + ", Destination:'"
        + (this.destination == null ? "unknown" : this.destination)
        + '\''
        + ", Delay:"
        + this.delay.toString()
        + ", Track:"
        + (this.track == -1 ? "unknown" : this.track);
  }
}
