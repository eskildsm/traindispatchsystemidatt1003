package edu.ntnu.stud;

import java.time.LocalTime;

/** Main class for the train dispatch application.
 *
 * @author 10031
 * @version 1.0
 */
public class TrainDispatchApp {
  /**
   * Main method for the train dispatch application.
   *
   * @param args command line arguments
   */
  public static void main(String[] args) {
    UserInterface userInterface = new UserInterface();
    userInterface.init();
    userInterface.start();
  }
}
