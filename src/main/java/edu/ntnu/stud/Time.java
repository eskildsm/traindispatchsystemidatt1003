package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * Represents the current time.
 *
 * <p>It is used to get and set the current time.</p>
 *
 * @author 10031
 * @version 1.0
 */
public class Time {
  private LocalTime currentTime;

  /**
   * Constructor to create a new instance of Time.
   *
   * @param currentTime - the current time
   * @throws NullPointerException if currentTime is null
   */
  public Time(LocalTime currentTime) throws NullPointerException {
    if (currentTime == null) {
      throw new NullPointerException("Current time cannot be null");
    }

    this.currentTime = LocalTime.now();
    setCurrentTime(currentTime);
  }

  /** Obtain the current time. */
  public LocalTime getCurrentTime() {
    return currentTime;
  }

  /**
   * Set the current time.
   *
   * @param currentTime the current time
   * @throws NullPointerException if currentTime is null
   */
  public void setCurrentTime(LocalTime currentTime) {
    if (currentTime == null) {
      throw new NullPointerException("Current time cannot be null");
    }
    this.currentTime = currentTime;
  }
}
