package edu.ntnu.stud;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * User input class for the train dispatch application. It reads the user input and throws errors if
 * values are invalid.
 *
 * @author 10031
 * @version 1.0
 */
public class UserInput {
  private final Scanner choice;
  private final TrainDepartureRegister trainDepartureRegister;

  /** Constructor for the user interface class. */
  public UserInput(TrainDepartureRegister trainDepartureRegister) {
    this.trainDepartureRegister = trainDepartureRegister;
    choice = new Scanner(System.in);
  }

  /** Obtain int input from user. */
  public int getIntInput() {
    this.choice.reset();
    return this.choice.nextInt();
  }

  /** Obtain string input from user. */
  public String getStringInput() {
    this.choice.reset();
    return this.choice.next();
  }

  /** Collect user choice from menus. */
  public int getChoice() {
    while (true) {
      System.out.println("Enter choice: ");
      try {
        return getIntInput();
      } catch (IllegalArgumentException | InputMismatchException e) {
        System.out.println("Invalid input, enter integer!");
        choice.next();
      }
    }
  }

  /**
   * Obtain unique departure number from user.
   *
   * @return unique departure number
   */
  public int getUniqueDepartureNumber() {
    int departureNumber;
    while (true) {
      System.out.println("Enter departure number: ");
      departureNumber = getIntInput();
      try {
        trainDepartureRegister.checkDepartureNumberUnique(departureNumber);
        break;
      } catch (IllegalArgumentException | InputMismatchException e) {
        System.out.println(e.getMessage());
      }
    }
    return departureNumber;
  }

  /**
   * Obtain existing departure number from user.
   *
   * @return existing departure number
   */
  public int getExistingDepartureNumber() {
    int departureNumber;
    while (true) {
      System.out.println("Enter departure number: ");
      departureNumber = getIntInput();
      try {
        trainDepartureRegister.searchForTrainDepartureByDepartureNumber(departureNumber);
        break;
      } catch (IllegalArgumentException | InputMismatchException e) {
        System.out.println(e.getMessage());
      }
    }
    return departureNumber;
  }

  /**
   * Obtain valid line from user.
   *
   * @return valid line
   */
  public String getValidLine() {
    String line;
    while (true) {
      System.out.println("Enter line: ");
      line = getStringInput();
      try {
        TrainDepartureRegister.checkLine(line);
        break;
      } catch (IllegalArgumentException | InputMismatchException e) {
        System.out.println(e.getMessage());
      }
    }
    return line;
  }

  /**
   * Obtain valid destination from user.
   *
   * @return valid destination
   */
  public String getValidDestination() {
    String destination;
    while (true) {
      System.out.println("Enter destination: ");
      destination = getStringInput();
      try {
        TrainDepartureRegister.checkDestination(destination);
        break;
      } catch (IllegalArgumentException | InputMismatchException e) {
        System.out.println(e.getMessage());
      }
    }
    return destination;
  }

  /**
   * Obtain valid track from user.
   *
   * @return valid track
   */
  public int getValidTrack() {
    int track;
    while (true) {
      System.out.println("Enter track (-1 to not set): ");
      track = getIntInput();
      try {
        TrainDepartureRegister.checkTrack(track);
        break;
      } catch (IllegalArgumentException | InputMismatchException e) {
        System.out.println(e.getMessage());
      }
    }
    return track;
  }

  /**
   * Obtain valid departure time from user.
   *
   * @return valid departure time
   */
  public LocalTime getValidDepartureTime() {
    LocalTime departureTime;
    while (true) {
      System.out.println("Enter departure time (format: HH:MM): ");
      String timeString = getStringInput();
      try {
        departureTime = LocalTime.parse(timeString);
        trainDepartureRegister.checkDepartureTime(departureTime);
        break;
      } catch (DateTimeParseException | IllegalArgumentException | InputMismatchException e) {
        System.out.println(e.getMessage());
      }
    }
    return departureTime;
  }

  /**
   * Obtain valid time from user.
   *
   * @param description description of what the input is
   * @return valid time
   */
  public LocalTime getValidTime(String description) {
    while (true) {
      System.out.println("Enter " + description + " (format: HH:MM): ");
      String delayString = getStringInput();
      try {
        return LocalTime.parse(delayString);
      } catch (DateTimeParseException | IllegalArgumentException | InputMismatchException e) {
        System.out.println("Invalid format: " + e.getMessage());
      }
    }
  }
}
